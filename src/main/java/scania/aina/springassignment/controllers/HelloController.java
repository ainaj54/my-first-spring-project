package scania.aina.springassignment.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping("/greeting")
    public String greeting(){
        String welcome = "Welcome to my greeting endpoint!";
        String name = "My name is Aina!";
        return welcome +name;
    }

    @GetMapping("/greeting/{yourName}")
    public String personalGreeting(@PathVariable String yourName){
        return "Hello " + yourName + ", my name is Aina. Welcome to my personalized endpoint";
    }

    @GetMapping("/palindrome")
    public String palindromeCheck(@RequestParam("query") String palindrome){
        String newPalindrome = palindrome.replaceAll("[^a-zA-Z0-9]", "");

        String reversedPalindrome = new StringBuilder(newPalindrome).reverse().toString();
        Boolean isPalindrom = true;
        for (int i = 0; i < newPalindrome.length()/2; i++) {
            if(newPalindrome.charAt(i) != reversedPalindrome.charAt(i)){
                isPalindrom = false;
                break;
            }
        }
        if(isPalindrom){
            return palindrome + " Is a Palindrome";
        }else{
            return palindrome + " Is not a Palindrome";
        }

    }
}
